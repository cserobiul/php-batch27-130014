<?php

    $array = array('First Name', 'Last Name', 'Address');
    
    echo '<pre>';
    print_r($array);
    
    $serializedData = serialize($array);
    
    echo '<pre>';
    echo $serializedData;
    
    $unseirializedData = unserialize($serializedData);
    echo '<pre>';
    print_r( $unseirializedData);

?>
